import glob
import os
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
plt.close('all')
import datetime
from datetime import date
import pandas as pd
import numpy as np
from scipy import signal

populationSWE = 10327589

# Pick the latest xlsx
fn = sorted(glob.glob('*.xlsx'), key=os.path.getmtime, reverse=True)[0]
xlsx = pd.ExcelFile(fn)

data = pd.read_excel(fn,sheet_name=xlsx.sheet_names[0])

fig = plt.figure()
plt.plot(data['Statistikdatum'],data['Totalt_antal_fall'].rolling(7).sum()/populationSWE*100000)
plt.title('7-day Incidence SWE - '+fn)
plt.grid()
fig.autofmt_xdate()
plt.savefig('foo.png')
