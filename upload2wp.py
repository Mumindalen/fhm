import requests
import os
import json
import uuid
from sys import argv

silent = False

def upload_image(wordpressUrl:str, filepath:str):
    APPLICATIONPASSWORD=os.environ['WP_PSW']
    USER=os.environ['WP_USR']
    # make a unique filename for the wordpress media library
    filename = uuid.uuid4().hex + os.path.splitext(filepath)[1]
    files = {'file': (filename, open(filepath,'rb'))}
    r = requests.post(url=wordpressUrl+"/wp-json/wp/v2/media",
    files=files,auth=(USER, APPLICATIONPASSWORD))
    try:
        silent or print(json.dumps(r.json(),sort_keys=True, indent=4))
        silent or print(r.status_code)
        silent or print(r.headers)
        imageId = r.json()['id']
        print('ID of uploaded image: '+str(imageId))
    except ValueError as err:
        print('Image upload failed '+str(err))
        imageId = None
    return imageId


if len(argv) < 3:
    print(f'Usage: {argv[0]} file_to_upload wordpress_url')
    exit(1)

imageId = upload_image(wordpressUrl=argv[2],filepath=argv[1])
